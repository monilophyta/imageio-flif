# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["PaletteTest"]

import os
import os.path
import tempfile

from PIL import Image

import numpy as np
import imageio

from .test_common import FlifTestCase
from ..src import flif_imageio_plugin


class PaletteTest( FlifTestCase ):

    def test_numpy_pil_conversion(self):
        img = self.rgb8_image
        pil_img = Image.fromarray( img, mode="RGB" )
        n_img = np.array( pil_img )
        self.assertEqual( (img == n_img).sum(), img.size )


    def test_palette(self):
        img = self.rgb8_image
        quantized_img, palette = self.quantize_rgb8_image( img, num_colors=123 )
        img = palette.take( quantized_img, axis=0 )
        fname = tempfile.mkstemp( suffix=".flif" )[1]
        
        try:
            # store image
            imageio.imwrite( fname, quantized_img, palette=palette )

            # load image with preserved palette
            flif_img_q = imageio.imread( fname, preserve_quantization=True )
            
            # load image without palette
            flif_img = imageio.imread( fname, preserve_quantization=False )
        finally:
            # delete file
            os.remove(fname)
        
        # get palette
        flif_img_palette = flif_img_q.meta["palette"]

        # check quantized image
        self.assertEqual( flif_img_q.shape, quantized_img.shape )
        self.assertEqual( flif_img_q.dtype, quantized_img.dtype )
        self.assertEqual( (flif_img_q == quantized_img).sum(), flif_img_q.size )
        self.assertEqual( flif_img_palette.shape, (palette.shape[0], 4) )
        
        if palette.shape[1] != 4:
            self.assertEqual( (flif_img_palette[:,3] == 255).sum(), flif_img_palette.shape[0] )
            self.assertEqual( (flif_img_palette[:,:3] == palette).sum(), palette.size )
        else:
            self.assertEqual( (flif_img_palette == palette).sum(), palette.size )

        self.assertEqual( flif_img.shape[:2], img.shape[:2] )
        self.assertGreaterEqual( flif_img.shape[2], img.shape[2] )
        self.assertEqual( flif_img.dtype, img.dtype )
        self.assertEqual( (flif_img[:,:,:3] == img[:,:,:3]).sum(), flif_img[:,:,:3].size )
    

    def quantize_rgb8_image(self, img, num_colors ):
        assert img.shape[-1] == 3
        mode = "RGB"

        pil_img = Image.fromarray( img, mode=mode )
        q_img = pil_img.quantize( colors=num_colors )

        palette = q_img.getpalette()
        num_channels = len(palette) // 256
        palette = np.array( palette, dtype=np.uint8 ).reshape((256,num_channels))
        palette = palette[:num_colors,:]

        q_img = np.array( q_img )
        assert np.all( q_img < num_colors )

        return ( q_img, palette)
