# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["FlifTestCase"]

import unittest

import numpy as np
import imageio


class FlifTestCase( unittest.TestCase ):
    
    # http://www.lenna.org/full/l_hires.jpg
    # http://www.lenna.org/full/len_full.jpg
    # http://www.lenna.org/lena_std.tif

    rand : np.random.RandomState

    # test image, float32
    chelsea_img : np.ndarray = None   # [ W x H x 3 ]

    # --------------------------------------------------------------------------------------------

    @property
    def gray8_image(self) -> np.ndarray:
        img = (self.chelsea_img.mean(axis=2) * 255).astype(np.uint8)
        return img
    
    @property
    def gray16_image(self) -> np.ndarray:
        img = (self.chelsea_img.mean(axis=2) * ((1<<16)-1)).astype(np.uint16)
        return img
    
    @property
    def rgb8_image(self) -> np.ndarray:
        img = (self.chelsea_img * 255).astype(np.uint8)
        return img
    
    @property
    def rgb16_image(self) -> np.ndarray:
        img = (self.chelsea_img * ((1<<16)-1)).astype(np.uint16)
        return img

    @property
    def rgba8_image(self) -> np.ndarray:
        img = (self.chelsea_img * 255).astype(np.uint8)
        alpha = self.rand.randint( 0, 256, size=img.shape[:2], dtype=np.uint8 )
        return np.concatenate( (img,alpha[:,:,np.newaxis]), axis=2 )
    
    @property
    def rgba16_image(self) -> np.ndarray:
        img = (self.chelsea_img * ((1<<16)-1)).astype(np.uint16)
        alpha = self.rand.randint( 0, 1<<16, size=img.shape[:2], dtype=np.uint16 )
        return np.concatenate( (img,alpha[:,:,np.newaxis]), axis=2 )
    
    # --------------------------------------------------------------------------------------------

    def __init__(self, *args, **argv):
        super(FlifTestCase, self).__init__(*args, **argv)

        if FlifTestCase.chelsea_img is None:
            img = imageio.imread('imageio:chelsea.png')
            FlifTestCase.chelsea_img = img.astype(np.float32) * (1. / 255)
        
        self.rand = np.random.RandomState(69)
    