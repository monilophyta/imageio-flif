# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["FlifAnimTest"]

import os
import os.path
import tempfile

import numpy as np
import imageio

from .test_common import FlifTestCase
from ..src import flif_imageio_plugin


imageio_images = [ "astronaut.png", "camera.png",
                   "checkerboard.png", "chelsea.png", "clock.png", "coffee.png", "coins.png",
                   "horse.png", "hubble_deep_field.png", "immunohistochemistry.png",
                   "moon.png", "page.png", "text.png", "wikkie.png" ]



class FlifAnimTest( FlifTestCase ):


    def test_flif_anim_gray8(self):
        orig_frames = self.create_image_sequence( self.gray8_image )
        self.generic_anim_test( orig_frames )


    def test_flif_anim_gray16(self):
        orig_frames = self.create_image_sequence( self.gray16_image )
        self.generic_anim_test( orig_frames )

    def test_flif_anim_rgb8(self):
        orig_frames = self.create_image_sequence( self.rgb8_image )
        self.generic_anim_test( orig_frames, disable_color_buckets=True )

    def test_flif_anim_rgba8_lossless_zero_alpha(self):
        orig_frames = self.create_image_sequence( self.rgba8_image )
        self.generic_anim_test( orig_frames, lossless_zero_alpha=True, disable_color_buckets=True )

    def test_flif_anim_rgba8_lossy_zero_alpha(self):
        orig_img = self.rgba8_image.copy()
        orig_img[:,:,-1].fill(125)
        orig_frames = self.create_image_sequence( orig_img )
        self.generic_anim_test( orig_frames, lossless_zero_alpha=False, disable_color_buckets=True )

    # ---------------------------------------------------------------------------------

    def generic_anim_test(self, orig_frames, **mimwrite_kw ):
        fname = tempfile.mkstemp( suffix=".flif" )[1]
        durations = 11 * np.arange( 1, len(orig_frames)+1 )

        try:
            # store image
            imageio.mimwrite( fname, orig_frames, duration=durations, **mimwrite_kw )

            # load image
            n_frames = imageio.mimread( fname )
        finally:
            # delete file
            os.remove(fname)
        for idx, (orig_img, flif_img) in enumerate( zip( orig_frames, n_frames ) ):
            self.assertEqual( flif_img.shape, orig_img.shape )
            self.assertEqual( flif_img.dtype, orig_img.dtype )
            self.assertEqual( (flif_img == orig_img).sum(), flif_img.size )
            self.assertTrue( "duration" in flif_img.meta )
            self.assertEqual( flif_img.meta["duration"], durations[idx] )

    # ---------------------------------------------------------------------------------

    @staticmethod
    def create_image_sequence( img ):
        image_seq = ( img, img[::-1], img[:,::-1], img[::-1,::-1] )
        return image_seq

    # ---------------------------------------------------------------------------------

    def get_gray8_iter(self):
        for img in self.get_image_iter():
            if img.ndim == 3:
                img = img.mean(axis=2)
            yield (img * 255).astype(np.uint8)
    
    # ---------------------------------------------------------------------------------

    def get_image_iter(self):
        for imname in imageio_images:
            try:
                img = imageio.imread('imageio:%s' % imname)
            except ValueError as err:
                continue

            img = img.astype(np.float32)
            img *= 1. / img.max()
            yield img

