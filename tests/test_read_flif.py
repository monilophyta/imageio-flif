# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--

__all__ = ["FlifIOTest"]

import os
import os.path
import tempfile

import numpy as np
import imageio

from .test_common import FlifTestCase
from ..src import flif_imageio_plugin


class FlifIOTest( FlifTestCase ):

    def test_gray8(self):
        self.generic_test( self.gray8_image )

    def test_gray16(self):
        self.generic_test( self.gray16_image )


    def test_rgb8(self):
        self.generic_test( self.rgb8_image )


    def test_rgba8_lossless_zero_alpha(self):
        self.generic_test( self.rgba8_image, lossless_zero_alpha=True )
    
    
    def test_rgba8_lossy_zero_alpha(self):
        orig_img = self.rgba8_image.copy()
        orig_img[:,:,-1].fill(125)
        self.generic_test( orig_img, lossless_zero_alpha=False )

    # ---------------------------------------------------------------------

    def generic_test(self, orig_img, **imwrite_kw ):
        fname = tempfile.mkstemp( suffix=".flif" )[1]
        
        try:
            # store image
            imageio.imwrite( fname, orig_img, **imwrite_kw )

            # load image
            flif_img = imageio.imread( fname )
        finally:
            # delete file
            os.remove(fname)
        
        self.assertEqual( flif_img.shape, orig_img.shape )
        self.assertEqual( flif_img.dtype, orig_img.dtype )
        self.assertEqual( (flif_img == orig_img).sum(), flif_img.size )
        self.assertTrue( "duration" in flif_img.meta )
