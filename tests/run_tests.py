# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["run_unit_tests"]


import unittest

from .test_read_flif import FlifIOTest
from .test_anim_flif import FlifAnimTest
from .test_palette import PaletteTest


def run_unit_tests():
    unittest.main()


if __name__ == '__main__':
    run_unit_tests()

