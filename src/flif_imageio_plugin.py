# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


from imageio import formats
from imageio.core import Format

from .flif_image_reading import FlifReaderBase
from .flif_image_writing import FlifWriterBase


class flifFormat(Format):
    """ Free Lossless Image Format (FLIF) is a lossless image format.
    Supports reading single frames and animations consisting of
       * gray images (single channel) at 8bit (uint8) or 16bit (uint16)
       * RGB images at 8bit (uint8) or 16bit (uint16) and with shape [WxHx3]
       * RGBA images at 8bit (uint8) or 16bit (uint16) and with shape [WxHx4]
       * quantized image with palettes of up to 256 RGBA entries at 8bit (uint8)
    
    Supports writing single frames and animations consisting of
       * gray images (single channel) at 8bit (uint8) or 16bit (uint16)
       * RGB images at 8bit (uint8) and with shape [WxHx3]
       * RGBA images at 8bit (uint8) and with shape [WxHx4]
       * quantized image with palettes of up to 256 RGBA entries at 8bit (uint8)

    When reading single or multiple images the meta data always contains
    a "duration" item.


    Parameters for reading
    ----------------------
    preserve_quantization : (Optional) bool
        Only relevant for images with quantization palette.
        The quantization palette (if relevant) becomes part of image 
        meta data dictionary: img.meta["palette"]
        Parameter values:
        True: preserve quantization indices in output image [WxH].
        False: (default) map quantization indices back into color space.
                The image becomes shape [WxHx4]

    Parameters for saving
    ---------------------
    lossless_zero_alpha : (Optional) bool
        True: keep RGB at A=0 (-K)
        False: not store RGB values of fully transparent pixels
                <=> no lossless ending where alpha is zero
    quality_loss : (Optional) int in range [-1:100]
                -1 : adaptive loss
                0 : lossless (default)
            1..100 : lossy encoding with maximum loss at 100
    crc_check : (Optional) bool
        True: add CRC to verify the integrity of the image data
    duration : (Optional) { None, int, sequence of int }
        The duration (in milliseconds) of each frame. Either specify
        one value that is used for all frames, or one value for each frame.
        None keeps default (0ms)
    disable_color_buckets : (Optional) bool
        True Corresponds to command line option "-B" and might be required  
        for compatibility with FLIF encoders before version 0.3
    palette : (Optional) {uint8 ndarray of shape [ <=256 x 3/4 ], list of palette arrays}
        Provides pre-defined palette for RGB(A)8 images/animations.
        The images then have to be indexed: each image needs to be a two-dimensional
        array [WxH] with dtype uint8.
        For animations a list of palettes (one for each animation frame)
        might be provided.
    """


    def __init__(self):
        super().__init__( name = "FLIF",
                   description = "FLIF, a lossless image format based on MANIAC compression (python wrapper)",
                    extensions = ["flif"],
                         modes = "iI" )

    def _can_read(self, request) -> bool:

        if request.mode[1] in (self.modes + "?"):
            if request.firstbytes[:4] == b'FLIF':
                return True
        return False


    def _can_write(self, request) -> bool:
        # In most cases, this code does suffice:
        if request.mode[1] in (self.modes + "?"):
            return True
        return False

    # -- reader

    class Reader(FlifReaderBase, Format.Reader):
        def __init__(self, *args, **argv):
            FlifReaderBase.__init__(self)
            Format.Reader.__init__(self, *args, **argv)

    # -- writer

    class Writer(FlifWriterBase, Format.Writer):
        def __init__(self, *args, **argv):
            FlifWriterBase.__init__(self)
            Format.Writer.__init__(self, *args, **argv)


# Register flifFormat as a imageio Format class.
formats.add_format(flifFormat())

