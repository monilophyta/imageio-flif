# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["FlifImage", "create_flif_image"]

import typing as tp
import ctypes as ct
import numpy as np

from .ctypes import flif_image_reader
from .ctypes import flif_image_writer



class FlifImage(object):

    handle        : ct.c_void_p
    has_ownership : bool


    def __init__( self, handle : ct.c_void_p, take_ownership : bool ):
        self.handle = handle
        self.has_ownership = bool(take_ownership)

    def __del__(self):
        if bool(self.handle) and self.has_ownership:
            flif_image_writer.destroy_image( self.handle )
            self.handle = 0
    
    def disown(self):
        self.has_ownership = False

    @property
    def width(self) -> int:
        return flif_image_reader.get_width( self.handle )
    
    @property
    def height(self) -> int:
        return flif_image_reader.get_height( self.handle )
    
    @property
    def nb_channels(self) -> int:
        return flif_image_reader.get_nb_channels( self.handle )
    
    @property
    def depth(self) -> int:
        return flif_image_reader.get_depth( self.handle )
    
    @property
    def palette_size(self) -> int:
        return flif_image_reader.get_palette_size( self.handle )

    @property
    def frame_delay(self) -> int:
        return flif_image_reader.get_frame_delay( self.handle )
    

    def get_palette(self) -> np.ndarray:
        assert self.palette_size > 0
        palette = np.empty( (self.palette_size,4), dtype=np.uint8, order="F" )
        assert np.isfortran( palette )

        palette_ptr = palette.ctypes.data_as( ct.c_void_p )
        flif_image_reader.get_palette( self.handle, palette_ptr )
        return palette


    def get_npy_image(self) -> np.ndarray:

        dtype = (np.uint8, np.uint16)
        depth = self.depth
        nb_channels = self.nb_channels

        if self.palette_size > 0:
            read_row = ( flif_image_reader.read_row_PALETTE8, None )
            img_shape = (self.height, self.width)
            depth = 8        # palette index of type uint8
            nb_channels = 1  # channels are part of palette
        
        elif nb_channels == 1:
            read_row = ( flif_image_reader.read_row_GRAY8, flif_image_reader.read_row_GRAY16 )
            img_shape = (self.height, self.width)
        
        else:
            assert self.nb_channels in {3,4}
            read_row = ( flif_image_reader.read_row_RGBA8, flif_image_reader.read_row_RGBA16 )
            img_shape = (self.height, self.width, 4)
        
        if depth == 8:
            dtype = dtype[0]
            read_row = read_row[0]
        elif depth == 16:
            dtype = dtype[1]
            read_row = read_row[1]
        else:
            assert False, "unexpected image depth %d, should be 8 or 16" % depth
        
        rshape = (img_shape[0], np.prod(img_shape[1:] ))

        image = np.empty( rshape, dtype=dtype, order="c" )
        assert image.flags['C_CONTIGUOUS'] is True
        assert (image.strides[0] % ((depth * self.width) // 8)) == 0
        
        # pointer to the beginning of the image
        img_ptr = image.ctypes.data_as( ct.c_void_p )

        # read image row-wise
        for ridx in range( image.shape[0] ):
            read_row( self.handle, ridx, img_ptr, image.strides[0] )
            img_ptr.value += image.strides[0]
        
        # set correct shape
        image = image.reshape( img_shape )

        # correct number of channels
        if nb_channels > 1:
            image = image[:,:,:nb_channels]

        return image    


    def set_frame_delay( self, delay : int ):
        delay = int(delay)
        if delay < 0:
            raise ValueError( "Invalid frame delay: %s ms" % delay )
        flif_image_writer.set_frame_delay( self.handle, delay )
        

# -----------------------------------------------------------------------------------------------------------------


def create_flif_image( image : np.ndarray,
                     palette : tp.Optional[np.ndarray] = None ) -> FlifImage:

    if image.size == 0:
        raise ValueError( "Unsupported image shape '%s'" % repr(image.shape) )
    
    if palette is not None:
        if not np.issubdtype( image.dtype, np.uint8 ):
            raise ValueError( "Image dtype (%r) not supported with palettes" % palette.dtype )
        if image.ndim != 2:
            raise ValueError( "Invalid shape (%s) of index image" % repr(image.shape) )

        palette = tidyup_palette( palette )
        importer = ( flif_image_writer.import_image_PALETTE, None )

    # check images planes
    elif 2 == image.ndim:
        # gray scale image
        importer = ( flif_image_writer.import_image_GRAY, flif_image_writer.import_image_GRAY16 )
    elif (3 == image.ndim) and (3 == image.shape[2]):
        # RGB Image
        importer = ( flif_image_writer.import_image_RGB, None )
    elif (3 == image.ndim) and (4 == image.shape[2]):
        # RGBA Image
        importer = ( flif_image_writer.import_image_RGBA, None )
    else:
        raise ValueError( "Unsupported image shape '%s'" % repr(image.shape) )
    
    if np.issubdtype( image.dtype, np.uint8 ):
        importer = importer[0]
    elif np.issubdtype( image.dtype, np.uint16 ):
        importer = importer[1]
    else:
        raise TypeError( "Unsupported image dtype %r" % image.dtype )
    
    if importer is None:
        raise ValueError( "Unsupported combination of channels size %d, dtype %r and palette %r" % (image.shape[-1], image.dtype, palette) )
    
    # verify image is C_CONTIGUOUS
    if (image.strides[0] < 0) or (not image[0].flags['C_CONTIGUOUS']):
        image = np.ascontiguousarray( image )
        assert image.flags['C_CONTIGUOUS'] is True

    # pointer to first pixel
    image_ptr = image.ctypes.data_as( ct.c_void_p )

    assert (image.strides[0] % image.itemsize) == 0
    image_row_stride = int(image.strides[0] // image.itemsize)
    assert image_row_stride >= 0, "FLIF library only support positive strides"

    image_handle = importer( image.shape[1], image.shape[0], image_ptr, image_row_stride )
    if not bool(image_handle):
        raise RuntimeError( "Error importing image of shape '%s'" % repr(image.shape) )
    
    # Set palette if existing
    if palette is not None:
        assert np.isfortran( palette )
        assert (palette.shape[0] <= 256) and (palette.shape[1] == 4)
        palette_ptr = palette.ctypes.data_as( ct.c_void_p )
        flif_image_writer.set_palette( image_handle, palette_ptr, palette.shape[0] )

    return FlifImage( image_handle, take_ownership=True )


def tidyup_palette( palette : np.ndarray ):

    if palette.ndim != 2:
        if palette.ndim == 1:
            raise ValueError( "Pallete not supported for gray scale images" )
        if palette.ndim > 2:
            raise ValueError( "Invalid palette shape %s" % repr(palette.shape) )
    
    if not np.issubdtype( palette.dtype, np.uint8 ):
        raise ValueError( "Palettes with dtype %r are not supported" % palette.dtype )

    if palette.shape[0] > 256:
        raise ValueError( "Palettes with more than 256 colors are not supported" )

    if palette.shape[1] not in {3,4}:
        raise ValueError( "Palettes are only supported with 3 or 4 channels")

    if palette.shape[1] == 3:
        palette = np.concatenate( ( palette,
                                    np.array([255], dtype=np.uint8).repeat( palette.shape[0] )[:,np.newaxis] ),
                                    axis=1 )
    
    return np.require( palette, requirements="F" )



