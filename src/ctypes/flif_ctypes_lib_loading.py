# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


import typing as tp
import ctypes as ct
import ctypes.util as ctutil

import glob
import os
import os.path
import fnmatch

import numpy as np

from .flif_ctypes_image_decoding import flif_decoder
from .flif_ctypes_image_encoding import flif_encoder
from .flif_ctypes_image_reading import flif_image_reader
from .flif_ctypes_image_writing import flif_image_writer


# ---------------------------------------------------------------------------------------


def find_library( search_dir ):

    assert os.path.isdir( search_dir )

    lib_fnames = [ "libflif.*" ]

    for lfname in lib_fnames:

        # recursively search in search_dir
        for dirpath, dirnames, filenames in os.walk( search_dir ):
            for filename in fnmatch.filter( filenames, lfname ):
                if  os.path.splitext( filename )[1] in {'.so', '.dll'}:
                    return os.path.join( dirpath, filename )
    
    return None


def load_flif_library():

    lib_path = ctutil.find_library( "flif" )

    if lib_path is None:
        this_path = os.path.abspath(__file__)
        for i in range(3):
            this_path = os.path.dirname( this_path )
        
        lib_path = find_library( this_path )

        if lib_path is None:
            repo_path = this_path
            for i in range(1):
                repo_path = os.path.dirname( repo_path )

            lib_path = find_library( repo_path )
            if lib_path is None:
                raise RuntimeError("libflif.{so,dll} binary not found")
        
    print("Loading library %s" % lib_path )
    lib_path, lib_fname = os.path.split( lib_path )
    fliflib = np.ctypeslib.load_library( lib_fname, lib_path )

    return fliflib


# ---------------------------------------------------------------------------------------

fliflib = load_flif_library()
flif_decoder.initialize( fliflib )
flif_encoder.initialize( fliflib )
flif_image_reader.initialize( fliflib )
flif_image_writer.initialize( fliflib )
