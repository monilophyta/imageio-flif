# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["flif_encoder"]


import typing as tp
import ctypes as ct


class flif_encoder(object):

    create_encoder         : tp.Callable[[],ct.c_void_p]
    destroy_encoder        : tp.Callable[[ct.c_void_p],tp.NoReturn]

    set_alpha_zero         : tp.Callable[[ct.c_void_p,int],tp.NoReturn]
    set_crc_check          : tp.Callable[[ct.c_void_p,int],tp.NoReturn]
    set_lossy              : tp.Callable[[ct.c_void_p,int],tp.NoReturn]
    set_auto_color_buckets : tp.Callable[[ct.c_void_p,int],tp.NoReturn]

    add_image        : tp.Callable[[ct.c_void_p,ct.c_void_p],tp.NoReturn]
    add_image_move   : tp.Callable[[ct.c_void_p,ct.c_void_p],tp.NoReturn]

    encode_file      : tp.Callable[[ct.c_void_p,bytes],int]


    @classmethod
    def initialize(cls, fliflib ):

        # FLIF_ENCODER* flif_create_encoder();
        cls.create_encoder = fliflib.flif_create_encoder
        cls.create_encoder.restype = ct.c_void_p

        # void flif_destroy_encoder(FLIF_ENCODER* encoder);
        cls.destroy_encoder = fliflib.flif_destroy_encoder
        cls.destroy_encoder.argtypes = [ ct.c_void_p ]


        cls.set_alpha_zero = fliflib.flif_encoder_set_alpha_zero
        cls.set_alpha_zero.argtypes = [ ct.c_void_p, ct.c_int32 ]

        cls.set_crc_check = fliflib.flif_encoder_set_crc_check
        cls.set_crc_check.argtypes = [ ct.c_void_p, ct.c_uint32 ]

        cls.set_lossy = fliflib.flif_encoder_set_lossy
        cls.set_lossy.argtypes = [ ct.c_void_p, ct.c_int32 ]

        # void flif_encoder_set_auto_color_buckets(FLIF_ENCODER* encoder, uint32_t acb);     // 0 = -B, 1 = default
        cls.set_auto_color_buckets = fliflib.flif_encoder_set_auto_color_buckets
        cls.set_auto_color_buckets.argtypes = [ ct.c_void_p, ct.c_uint32 ]


        # void flif_encoder_add_image(FLIF_ENCODER* encoder, FLIF_IMAGE* image);
        cls.add_image = fliflib.flif_encoder_add_image
        cls.add_image.argtypes = [ ct.c_void_p, ct.c_void_p ]

        # void flif_encoder_add_image_move(FLIF_ENCODER* encoder, FLIF_IMAGE* image);
        cls.add_image_move = fliflib.flif_encoder_add_image_move
        cls.add_image_move.argtypes = [ ct.c_void_p, ct.c_void_p ]
        
        # int32_t flif_encoder_encode_file(FLIF_ENCODER* encoder, const char* filename);
        cls.encode_file = fliflib.flif_encoder_encode_file
        cls.encode_file.argtypes = [ ct.c_void_p, ct.c_char_p ]
        cls.encode_file.restype = ct.c_int32

