# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["flif_image_reader"]


import typing as tp
import ctypes as ct



class flif_image_reader(object):

    get_width         : tp.Callable[[ct.c_void_p],int]
    get_height        : tp.Callable[[ct.c_void_p],int]
    get_nb_channels   : tp.Callable[[ct.c_void_p],int]
    get_depth         : tp.Callable[[ct.c_void_p],int]
    get_palette_size  : tp.Callable[[ct.c_void_p],int]
    get_frame_delay   : tp.Callable[[ct.c_void_p],int]

    get_palette       : tp.Callable[[ct.c_void_p,ct.c_void_p],tp.NoReturn]

    read_row_PALETTE8 : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    read_row_GRAY8    : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    read_row_GRAY16   : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    read_row_RGBA8    : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]
    read_row_RGBA16   : tp.Callable[[ct.c_void_p,int,ct.c_void_p,int],tp.NoReturn]


    @classmethod
    def initialize(cls, fliflib ):

        # uint32_t flif_image_get_width(FLIF_IMAGE* image);
        cls.get_width = fliflib.flif_image_get_width
        cls.get_width.argtypes = [ ct.c_void_p ]
        cls.get_width.restype = ct.c_uint32

        # uint32_t flif_image_get_height(FLIF_IMAGE* image);
        cls.get_height = fliflib.flif_image_get_height
        cls.get_height.argtypes = [ ct.c_void_p ]
        cls.get_height.restype = ct.c_uint32

        # uint8_t flif_image_get_nb_channels(FLIF_IMAGE* image);
        cls.get_nb_channels = fliflib.flif_image_get_nb_channels
        cls.get_nb_channels.argtypes = [ ct.c_void_p ]
        cls.get_nb_channels.restype = ct.c_uint8

        # uint8_t flif_image_get_depth(FLIF_IMAGE* image);
        cls.get_depth = fliflib.flif_image_get_depth
        cls.get_depth.argtypes = [ ct.c_void_p ]
        cls.get_depth.restype = ct.c_uint8

        # uint32_t flif_image_get_palette_size(FLIF_IMAGE* image); // 0 = no palette, 1-256 = nb of colors in palette
        cls.get_palette_size = fliflib.flif_image_get_palette_size
        cls.get_palette_size.argtypes = [ ct.c_void_p ]
        cls.get_palette_size.restype = ct.c_uint32

        # uint32_t flif_image_get_frame_delay(FLIF_IMAGE* image);
        cls.get_frame_delay = fliflib.flif_image_get_frame_delay
        cls.get_frame_delay.argtypes = [ ct.c_void_p ]
        cls.get_frame_delay.restype = ct.c_uint32

        # uint8_t flif_image_get_metadata(FLIF_IMAGE* image, const char* chunkname, unsigned char** data, size_t* length);


        # void flif_image_get_palette(FLIF_IMAGE* image, void* buffer);  // puts RGBA colors in buffer (4*palette_size bytes)
        cls.get_palette = fliflib.flif_image_get_palette
        cls.get_palette.argtypes = [ ct.c_void_p, ct.c_void_p ]

        # void flif_image_read_row_PALETTE8(FLIF_IMAGE* image, uint32_t row, void* buffer, size_t buffer_size_bytes);
        cls.read_row_PALETTE8 = fliflib.flif_image_read_row_PALETTE8
        cls.read_row_PALETTE8.argtypes = [ ct.c_void_p, ct.c_uint32, ct.c_void_p, ct.c_size_t ]

        # void flif_image_read_row_GRAY8(FLIF_IMAGE* image, uint32_t row, void* buffer, size_t buffer_size_bytes);
        cls.read_row_GRAY8 = fliflib.flif_image_read_row_GRAY8
        cls.read_row_GRAY8.argtypes = cls.read_row_PALETTE8.argtypes

        # void flif_image_read_row_GRAY16(FLIF_IMAGE* image, uint32_t row, void* buffer, size_t buffer_size_bytes);
        cls.read_row_GRAY16 = fliflib.flif_image_read_row_GRAY16
        cls.read_row_GRAY16.argtypes = cls.read_row_PALETTE8.argtypes
    
        # void flif_image_read_row_RGBA8(FLIF_IMAGE* image, uint32_t row, void* buffer, size_t buffer_size_bytes);
        cls.read_row_RGBA8 = fliflib.flif_image_read_row_RGBA8
        cls.read_row_RGBA8.argtypes = cls.read_row_PALETTE8.argtypes

        # void flif_image_read_row_RGBA16(FLIF_IMAGE* image, uint32_t row, void* buffer, size_t buffer_size_bytes);
        cls.read_row_RGBA16 = fliflib.flif_image_read_row_RGBA16
        cls.read_row_RGBA16.argtypes = cls.read_row_PALETTE8.argtypes

