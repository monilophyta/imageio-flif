# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["flif_decoder"]


import typing as tp
import ctypes as ct


class flif_decoder(object):

    create_decoder       : tp.Callable[[],ct.c_void_p]
    destroy_decoder      : tp.Callable[[ct.c_void_p],tp.NoReturn]
    abort_decoder        : tp.Callable[[ct.c_void_p],int]
    
    num_images           : tp.Callable[[ct.c_void_p],int]
    num_loops            : tp.Callable[[ct.c_void_p],int]

    #set_crc_check        : tp.Callable
    #set_quality          : tp.Callable
    #set_scale            : tp.Callable
    #set_resize           : tp.Callable
    #set_fit              : tp.Callable
    
    decode_file          : tp.Callable[[ct.c_void_p,bytes],int]
    get_image_handle     : tp.Callable[[ct.c_void_p,int],ct.c_void_p]


    @classmethod
    def initialize(cls, fliflib ):
        
        # FLIF_DECODER* flif_create_decoder();
        cls.create_decoder = fliflib.flif_create_decoder
        cls.create_decoder.restype = ct.c_void_p

        # void flif_destroy_decoder(FLIF_DECODER* decoder);
        cls.destroy_decoder = fliflib.flif_destroy_decoder
        cls.destroy_decoder.argtypes = [ ct.c_void_p ]
        
        # int32_t flif_abort_decoder(FLIF_DECODER* decoder);
        cls.abort_decoder = fliflib.flif_abort_decoder
        cls.abort_decoder.argtypes = [ ct.c_void_p ]
        cls.abort_decoder.restype = ct.c_int32

        # size_t flif_decoder_num_images(FLIF_DECODER* decoder);
        cls.num_images = fliflib.flif_decoder_num_images
        cls.num_images.argtypes = [ ct.c_void_p ]
        cls.num_images.restype = ct.c_size_t

        # int32_t flif_decoder_num_loops(FLIF_DECODER* decoder);
        cls.num_loops = fliflib.flif_decoder_num_images
        cls.num_loops.argtypes = [ ct.c_void_p ]
        cls.num_loops.restype = ct.c_int32

        # int32_t flif_decoder_decode_filepointer(FLIF_DECODER* decoder, FILE *filepointer, const char *filename);
        #cls.decode_filepointer = fliflib.flif_decoder_decode_filepointer
        #cls.decode_filepointer.argtypes = [ ct.c_void_p, ct.c_void_p, ct.c_char_p ]
        #cls.decode_filepointer.restype = ct.c_int32

        #int32_t flif_decoder_decode_file(FLIF_DECODER* decoder, const char* filename);
        cls.decode_file = fliflib.flif_decoder_decode_file
        cls.decode_file.argtypes = [ct.c_void_p, ct.c_char_p]
        cls.decode_file.restype = ct.c_int32

        # FLIF_IMAGE* flif_decoder_get_image(FLIF_DECODER* decoder, size_t index);
        cls.get_image_handle = fliflib.flif_decoder_get_image
        cls.get_image_handle.argtypes = [ ct.c_void_p, ct.c_size_t ]
        cls.get_image_handle.restype = ct.c_void_p








