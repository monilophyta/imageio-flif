#
# Copyright (c) 2020 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = ["FlifWriterBase"]



import typing as tp
import ctypes as ct
import collections.abc
import itertools as it

import warnings

import numpy as np
from .ctypes import flif_encoder
from .flif_image import FlifImage, create_flif_image


class FlifWriterBase( object ):

    __encoder_handle : tp.Optional[ct.c_void_p]
    __duration       : tp.Optional[tp.Union[int,list]]

    __image_shape : tp.Optional[tuple]
    __image_dtype : tp.Optional[np.dtype]
    __palette     : tp.Optional[tp.Union[list,np.ndarray]]

    __num_images : int


    def __init__(self):
        #super().__(*args, **argv)
        self.__encoder_handle = None


    def _open(self, lossless_zero_alpha : bool = False,
                           quality_loss : int = 0,
                              crc_check : bool = True,
                  disable_color_buckets : bool = False,
                               duration : tp.Optional[tp.Union[int,list]] = None,
                                palette : tp.Optional[tp.Union[list,np.ndarray]] = None ):
        """
        Parameters for saving
        ---------------------
        lossless_zero_alpha : (Optional) bool
            True: keep RGB at A=0 (-K)
            False: not store RGB values of fully transparent pixels
                    <=> no lossless ending where alpha is zero
        quality_loss : (Optional) int in range [-1:100]
                 -1 : adaptive loss
                  0 : lossless (default)
             1..100 : lossy encoding with maximum loss at 100
        crc_check : (Optional) bool
            True: add CRC to verify the integrity of the image data
        duration : (Optional) { None, int, sequence of int }
            The duration (in milliseconds) of each frame. Either specify
            one value that is used for all frames, or one value for each frame.
            None keeps default (0ms)
        disable_color_buckets : (Optional) bool
            True Corresponds to command line option "-B" and might be required  
            for compatibility with FLIF encoders before version 0.3
        palette : (Optional) {uint8 ndarray of shape [ <=256 x 3/4 ], list of palette arrays}
            Provides pre-defined palette for RGB(A)8 images/animations.
            The images then have to be indexed: each image needs to be a two-dimensional
            array [WxH] with dtype uint8.
            For animations a list of palettes (one for each animation frame)
            might be provided.
        """
        # create encoder
        encoder_handle = flif_encoder.create_encoder()
        if not bool(encoder_handle):
            raise MemoryError("Error creating FLIF encoder")
        
        ### set parameters ######

        if bool(lossless_zero_alpha):
            flif_encoder.set_alpha_zero( encoder_handle, 1 )
        else:
            flif_encoder.set_alpha_zero( encoder_handle, 0 )
        
        quality_loss = max( -1, min( 100, int(quality_loss) ) )
        flif_encoder.set_lossy( encoder_handle, quality_loss )

        if bool(crc_check):
            flif_encoder.set_crc_check( encoder_handle, 1 )
        else:
            flif_encoder.set_crc_check( encoder_handle, 0 )
        
        if bool(disable_color_buckets):
            flif_encoder.set_auto_color_buckets( encoder_handle, 0 )
        else:
            flif_encoder.set_auto_color_buckets( encoder_handle, 1 ) # default

        self.__encoder_handle = encoder_handle

        if duration is not None:
            if isinstance( duration, collections.abc.Iterable ):
                duration = it.cycle( duration )
            else:
                duration = it.repeat( int(duration) )
        self.__duration = duration

        if palette is not None:
            if isinstance( palette, np.ndarray ):
                palette = it.repeat( palette )
            elif isinstance( palette, list ):
                if not isinstance( palette[0], np.ndarray ):
                    raise ValueError( "Invalid value for parameter 'palette'")
                palette = it.cycle( palette )
        self.__palette = palette

        self.__image_dtype = None
        self.__image_shape = None
        self.__num_images = 0

        

    def _close(self):
        if (self.__encoder_handle is not None) and (self.__num_images > 0):
            filename = self.request.get_local_filename()
            
            retval = flif_encoder.encode_file( self.__encoder_handle, filename.encode('utf-8') )
            
            # destroy encoder in any case
            flif_encoder.destroy_encoder( self.__encoder_handle )
            self.__encoder_handle = None

            if retval != 1:
                raise IOError("Error writing FLIF file %s" % filename )


    def _append_data(self, image, meta=None):
        # Process the given data and meta data.
        if __debug__ and (meta is not None) and (len(meta) > 0):
            warnings.warn("Writing of meta data not (yet) supported", RuntimeWarning)
            #print(meta)

        if self.__image_shape is None:
            self.__image_shape = image.shape
            self.__image_dtype = image.dtype
        elif self.__image_shape != image.shape:
            raise ValueError( "Shape %s does not match with previous image shape %r" % (repr(image.shape), repr(self.__image_shape)) )
        elif self.__image_dtype != image.dtype:
            raise ValueError( "dtype %r does not match with previous image dtype %r" % (image.dtype, self.__image_dtype) )
        
        palette = self.__palette
        if palette is not None:
            palette = next(palette)

        flif_image = create_flif_image( image, palette=palette )

        if self.__duration is not None:
            duration = int(next(self.__duration))
            flif_image.set_frame_delay( duration )

        flif_encoder.add_image_move( self.__encoder_handle, flif_image.handle )
        flif_image.disown()
        self.__num_images += 1


    def set_meta_data(self, meta):
        # Process the given meta data (global for all images)
        # It is not mandatory to support this.
        raise RuntimeError("Writing of meta data not (yet) supported")
